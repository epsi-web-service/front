const common = require('./webpack.prod.js')
const merge = require('webpack-merge')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = merge(common, {
  plugins: [new BundleAnalyzerPlugin()],
})
