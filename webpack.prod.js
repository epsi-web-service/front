const path = require('path')
const merge = require('webpack-merge')
const webpack = require('webpack')

// Webpack plugins
const HtmlPlugin = require('html-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const ImageminPlugin = require('imagemin-webpack-plugin').default
const RobotsTxtPlugin = require('robotstxt-webpack-plugin').default
const DotenvPlugin = require('dotenv-webpack')
const PrerenderSPAPlugin = require('prerender-spa-plugin')

const robotsTxtOptions = require('./robots-txt.config')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          ecma: 8,
          warnings: false,
          compress: {
            warnings: false,
            conditionals: true,
            unused: true,
            comparisons: true,
            sequences: true,
            dead_code: true,
            evaluate: true,
            if_return: true,
            join_vars: true,
            drop_console: true,
            drop_debugger: true,
          },
          output: {
            comments: false,
            beautify: false,
          },
          sourceMap: false,
          pure_funcs: ['console.log'],
          toplevel: false,
          nameCache: null,
          ie8: false,
          keep_classnames: false,
          keep_fnames: false,
          safari10: false,
        },
      }),
    ]
  },
  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.HashedModuleIdsPlugin(),
    new HtmlPlugin({
      template: './src/index.html',
      excludeChunks: ['base'],
      filename: 'index.html',
      minify: {
        collapseWhitespace: true,
        collapseInlineTagWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
      },
    }),
    new ImageminPlugin({
      test: /\.(jpe?g|png|gif|svg)$/i,
      optipng: {
        optimizationLevel: 7,
      },
      pngquant: {
        quality: '65-90',
        speed: 4,
      },
      gifsicle: {
        optimizationLevel: 3,
      },
      svgo: {
        plugins: [
          {
            removeViewBox: false,
            removeEmptyAttrs: true,
          },
        ],
      },
      jpegtran: {
        progressive: true,
      },
    }),
    new RobotsTxtPlugin(robotsTxtOptions),
    new CompressionPlugin({
      filename: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|/,
      threshold: 10240,
      minRatio: 0.8,
    }),
    new DotenvPlugin({
      systemvars: true,
    }),
    new PrerenderSPAPlugin({
      staticDir: path.resolve(__dirname, 'dist'),
      routes: [ '/' ],
      renderer: new PrerenderSPAPlugin.PuppeteerRenderer({
        renderAfterTime: 5000,
        args: ['--lang=fr-FR,fr'],
      }),
    }),
  ],
})
