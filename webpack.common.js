const path = require('path')
const webpack = require('webpack')
const env = require('dotenv')
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin')

env.config()

const excludedFolders = [path.join(__dirname, 'node_modules'), /flexboxgrid/]

module.exports = {
  entry: {
    client: ['@babel/polyfill', 'react-hot-loader/patch', './src/index.js'],
    vendor: ['react', 'react-dom', 'react-router-dom', 'redux', 'redux-observable', 'react-redux', 'rxjs'],
  },
  output: {
    filename: 'assets/scripts/[name].[hash].js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
  },
  module: {
    rules: [
      // JS
      {
        test: /\.js$/,
        exclude: excludedFolders,
        use: 'babel-loader',
      },
      // CSS
      {
        test: /assets(\/|\\).*\.css$/,
        use: [
          ExtractCssChunks.loader,
          'css-loader',
          'postcss-loader',
        ],
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader',
        include: [/flexboxgrid/, /react-times/, /react-day-picker/],
      },
      // Fonts
      {
        test: /fonts(\/|\\).*\.(woff(2)?|ttf)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              publicPath: './../fonts/',
              outputPath: 'assets/fonts/',
              name: '[name].[ext]',
            },
          },
        ],
      },
      // Images
      {
        test: /\.(jpe?g|png)$/,
        loader: 'responsive-loader',
        options: {
          name: 'assets/images/[hash]-[width].[ext]',
          adapter: require('responsive-loader/sharp'),
          placeholder: true,
        },
      },
      {
        test: /\.gif$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[hash].[ext]',
              outputPath: 'assets/images/',
            },
          },
        ],
      },
      // SVG
      {
        test: /img(\/|\\).*\.svg$/,
        use: [
          {
            loader: 'svg-loader',
          },
        ],
      },
      // YAML
      {
        test: /\.yaml$/,
        use: [{ loader: 'json-loader' }, { loader: 'yaml-loader' }],
      },
    ],
  },
  plugins: [
    new webpack.IgnorePlugin(/caniuse-lite\/data\/regions/),
    new ExtractCssChunks({
      filename: 'assets/stylesheets/[name].[hash].css',
      hot: true,
    }),
  ],
  resolve: {
    modules: [path.resolve('./src'), path.resolve('./node_modules')],
  },
}
