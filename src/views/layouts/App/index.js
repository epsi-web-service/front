import React, { Component, Fragment } from 'react'
import { Route } from 'react-router-dom'
import { hot } from 'react-hot-loader'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import routes from 'routes'

const mapStateToProps = (state) => ({
  authenticated: state.authentication.authenticated,
  ready: state.authentication.ready,
  location: state.router.location,
})

class App extends Component {
  static propTypes = {
    authenticated: PropTypes.bool,
    ready: PropTypes.bool,
    location: PropTypes.object.isRequired,
  }

  render() {
    const { authenticated } = this.props
    return (
      <Fragment>
        <main>
          {routes.map((route) => (
            <Route key={route.path} {...route} />
          ))}
        </main>
      </Fragment>
    )
  }
}

export default hot(module)(
  connect(
    mapStateToProps,
    () => ({}),
    null,
    { pure: false },
  )(App),
)
