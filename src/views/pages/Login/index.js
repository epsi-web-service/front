import React, {Component} from 'react'
import { NavLink } from 'react-router-dom'
import { required, email } from 'redux-form-validators'
import InputText from 'views/components/presentationals/InputText'
import { reduxForm, Field } from 'redux-form'
import { connect } from 'react-redux'
import { loginRequest } from 'store/ducks/authentication'

class Login extends Component {
    render() {
      const { handleSubmit, onSubmit, intl, className, done, loading, errors, initialValues } = this.props

      return (
          <div className="login-dark">
              <h3>ToDo List Login</h3>
              <form onSubmit={handleSubmit(onSubmit)} className="form">
                  {
                      errors.length > 0 ?
                          <div className="alert alert-danger" role="alert">
                              {errors}
                          </div>
                          :
                          null
                  }
                  <div className="form-group">
                      <Field
                          name="email"
                          type="text"
                          className="form-control"
                          placeholder="Email"
                          component="input"
                      />
                  </div>
                  <div className="form-group">
                      <Field
                          name="password"
                          className="form-control"
                          type="password"
                          placeholder="Password"
                          component="input"
                      />
                  </div>
                  <div className="form-group">
                      <button disabled={loading} className="btn btn-primary btn-block" type="submit">Connexion</button>
                  </div>
                  <NavLink to={'register'} className={"forgot"}>Sign up</NavLink>
              </form>
          </div>
      )
    }
}

const mapStateToProps = (state) => ({
    loading: state.authentication.loading,
    errors: state.authentication.errors,
})

const mapDispatchToProps = (dispatch) => ({
    onSubmit: (payload) => dispatch(loginRequest(payload.email, payload.password)),
})

const formed = reduxForm({ form: 'login', enableReinitialize: false })(Login)
const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(formed)

export default connected
