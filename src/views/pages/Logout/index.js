import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { logoutRequest } from 'store/ducks/authentication'
import {LOCALSTORAGE_REFRESH_TOKEN} from "../../../store/ducks/authentication";

import { resetTodos } from 'store/ducks/todolist'


const mapStateToProps = (state) => ({})
const mapDispatchToProps = (dispatch) => ({
  onLoad: () => dispatch(logoutRequest()),
  resetTodos: () => dispatch(resetTodos())
})

class Logout extends PureComponent {
  static propTypes = {
    onLoad: PropTypes.func.isRequired,
  }

  componentDidMount() {
    localStorage.removeItem(LOCALSTORAGE_REFRESH_TOKEN)
    this.props.onLoad && this.props.onLoad()
    this.props.resetTodos()
  }

  render() {
    return <Redirect to="/" />
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Logout)
