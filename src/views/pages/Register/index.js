import React, {Component} from 'react'
import {NavLink} from "react-router-dom";
import { reduxForm, Field } from 'redux-form'
import { connect } from 'react-redux'
import { registerRequest } from 'store/ducks/register'

class Register extends Component {
    render() {
        const { handleSubmit, onSubmit, intl, className, done, loading, errors, initialValues } = this.props

        return (
            <div className="login-dark">
                <h3>ToDo List Register</h3>
                <form onSubmit={handleSubmit(onSubmit)} className="form">
                    {
                        errors.length > 0 ?
                            <div className="alert alert-danger" role="alert">
                                {errors}
                            </div>
                            :
                            null
                    }
                    <div className="form-group">
                        <Field
                            name="email"
                            type="text"
                            className="form-control"
                            placeholder="Email"
                            component="input"
                        />
                    </div>
                    <div className="form-group">
                        <Field
                            name="password"
                            className="form-control"
                            type="password"
                            placeholder="Password"
                            component="input"
                        />
                    </div>
                    <div className="form-group">
                        <button disabled={loading} className="btn btn-primary btn-block" type="submit">Sign up</button>
                    </div>
                    <NavLink to={'/'} className={"forgot"}>Login</NavLink>
                </form>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    loading: state.register.signup.loading,
    errors: state.register.signup.violations,
})

const mapDispatchToProps = (dispatch) => ({
    onSubmit: (payload) => dispatch(registerRequest({email: payload.email, password: payload.password})),
})

const formed = reduxForm({ form: 'register', enableReinitialize: false })(Register)
const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(formed)

export default connected
