import React, {Component, Fragment} from 'react'
import {NavLink} from "react-router-dom";
import { connect } from 'react-redux'
import { postTodoRequest, patchTodoToggleRequest, deleteTodoRequest, updateTodoDescriptionLocal, patchTodoDescriptionRequest } from 'store/ducks/todolist'

class TodoList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleAddTodo = this.handleAddTodo.bind(this);
        this.handleChangeTodoToggle = this.handleChangeTodoToggle.bind(this);
        this.handleDeleteTodo = this.handleDeleteTodo.bind(this);
        this.handleChangeTodoDescription = this.handleChangeTodoDescription.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleAddTodo () {
        this.props.postTodo(this.state.value)
        this.setState({value: ''});
    }

    handleDeleteTodo (id) {
        this.props.deleteTodo(id)
    }

    handleUpdateTodo (id) {
        this.props.updateTodoDescription(id)
    }

    handleChangeTodoDescription = id => event => {
        this.props.updateTodoDescriptionLocal(id, event.target.value)
    }

    handleChangeTodoToggle = id => event => {
        this.props.updateTodoToggle(id, event.target.value)
    }

    render() {

        let { user, todoList } = this.props

        return (
            <Fragment>
                <div id="todo-list" className="dark">
                    <div className="container">
                        <h3>Hello {user.username} <NavLink to={'logout'} className={"forgot"}>Logout</NavLink> </h3>
                        <p>
                            <label htmlFor="new-task">Add Item</label><input id="new-task" type="text" onChange={this.handleChange} value={this.state.value}/>
                            <button onClick={this.handleAddTodo}>Add</button>
                        </p>
                        <h3>To do</h3>
                        <ul id="incomplete-tasks">
                            {
                                todoList
                                    .filter((todo) => todo.done === false)
                                    .map((todo) =>
                                    <li key={todo.id}>
                                        <input type="checkbox" value={todo.done} onChange={this.handleChangeTodoToggle(todo.id)} />
                                        <input type="text" onChange={this.handleChangeTodoDescription(todo.id)} value={todo.description} />
                                        <button onClick={() => this.handleUpdateTodo(todo.id)}>Update</button>
                                        <button onClick={() => this.handleDeleteTodo(todo.id)}>Delete</button>
                                    </li>
                                )
                            }
                        </ul>
                        <h3>Completed</h3>
                        <ul id="completed-tasks">
                            {
                                todoList
                                    .filter((todo) => todo.done === true)
                                    .map((todo) =>
                                        <li key={todo.id}>
                                            <input type="checkbox" value={todo.done} onChange={this.handleChangeTodoToggle(todo.id)} checked/>
                                            <input type="text" onChange={this.handleChangeTodoDescription(todo.id)} value={todo.description} />
                                            <button onClick={() => this.handleUpdateTodo(todo.id)}>Update</button>
                                            <button onClick={() => this.handleDeleteTodo(todo.id)}>Delete</button>
                                        </li>
                                    )
                            }
                        </ul>
                    </div>
                </div>
            </Fragment>
        )
    }
}


const mapStateToProps = (state) => ({
    user: state.authentication.payload,
    todoList: state.todolist.todos
})

const mapDispatchToProps = (dispatch) => ({
    postTodo: (name) => dispatch(postTodoRequest(name)),
    updateTodoDescriptionLocal: (id, value) => dispatch(updateTodoDescriptionLocal(id,value)),
    updateTodoDescription: (id) => dispatch(patchTodoDescriptionRequest(id)),
    updateTodoToggle: (id, value) => dispatch(patchTodoToggleRequest(id, value)),
    deleteTodo: (id) => dispatch(deleteTodoRequest(id)),
})

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(TodoList)

export default connected

