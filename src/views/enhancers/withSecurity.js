import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";


export default (WrappedComponent) => {
    const WithSecurity = props => {
        if (!props.isGranted) {
            return <Redirect to="/" />;
        }

        return <WrappedComponent {...props} />;
    };

    const mapStateToProps = state => ({
        isGranted: state.authentication.authenticated,
    });

    return connect(mapStateToProps)(WithSecurity);
};
