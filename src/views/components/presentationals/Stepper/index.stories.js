import React from 'react'
import { storiesOf } from '@storybook/react' // eslint-disable-line no-unused-vars

// add-on
import { withInfo } from '@storybook/addon-info'
import { number } from '@storybook/addon-knobs'

// component
import Stepper from './'

storiesOf('Stepper', module)
  .add(
    'Usage',
    withInfo(`
      #### Import
      ~~~js
      import Stepper from 'views/components/presentationals/Stepper'
      ~~~
      ---
      #### Usage
      ~~~js
        <Stepper
          currentStep={2}
          spaced={0}
          steps={[
            {
              name: 'Step 1',
              icon: 'status',
            },
            {
              name: 'Step 2',
              icon: 'societe',
            },
            {
              name: 'Step 3',
              icon: 'informations',
            },
            {
              name: 'Step 4',
              icon: 'aide',
            },
          ]}
        />
      ~~~

    `)(() => (
      <div style={{ marginLeft: '50px' }}>
        <Stepper
          spaced={number('Spacing (in px)', 0)}
          currentStep={number('current step', 2)}
          steps={[
            {
              name: 'Step 1',
              icon: 'status',
            },
            {
              name: 'Step 2',
              icon: 'societe',
            },
            {
              name: 'Step 3',
              icon: 'informations',
            },
            {
              name: 'Step 4',
              icon: 'aide',
            },
          ]}
        />
      </div>
    )),
  )
  .add(
    'Register',
    withInfo(`
      #### Import
      ~~~js
      import Stepper from 'views/components/presentationals/Stepper'
      ~~~
      ---
      #### Usage
      ~~~js
        <Stepper
          currentStep={1}
          steps={[
            {
              name: 'Votre statut',
              icon: 'status',
            },
            {
              name: 'Votre société',
              icon: 'societe',
            },
            {
              name: 'Vos informations',
              icon: 'informations',
            },
          ]}
        />
      ~~~

    `)(() => (
      <div style={{ marginLeft: '50px' }}>
        <Stepper
          currentStep={number('current step', 1)}
          steps={[
            {
              name: 'Votre statut',
              icon: 'status',
            },
            {
              name: 'Votre société',
              icon: 'societe',
            },
            {
              name: 'Vos informations',
              icon: 'informations',
            },
          ]}
        />
      </div>
    )),
  )
  .add(
    'Delivery',
    withInfo(`
      #### Import
      ~~~js
      import Stepper from 'views/components/presentationals/Stepper'
      ~~~
      ---
      #### Usage
      ~~~js
        <Stepper
          currentStep={1}
          steps={[
            {
              name: 'La marchandise',
              icon: 'marchandise',
            },
            {
              name: 'Le véhicule',
              icon: 'vehicule',
            },
            {
              name: 'Le trajet',
              icon: 'location',
            },
            {
              name: 'Finalisation',
              icon: 'recapitulatif',
            },
          ]}
        />
      ~~~

    `)(() => (
      <div style={{ marginLeft: '50px' }}>
        <Stepper
          currentStep={1}
          steps={[
            {
              name: 'La marchandise',
              icon: 'marchandise',
            },
            {
              name: 'Le véhicule',
              icon: 'vehicule',
            },
            {
              name: 'Le trajet',
              icon: 'location',
            },
            {
              name: 'Finalisation',
              icon: 'recapitulatif',
            },
          ]}
        />
      </div>
    )),
  )
  .add(
    'Monitoring',
    withInfo(`
      #### Import
      ~~~js
      import Stepper from 'views/components/presentationals/Stepper'
      ~~~
      ---
      #### Usage
      ~~~js
        <Stepper
          currentStep={1}
          spaced={45}
          steps={[
            {
              name: 'Départ',
              icon: 'marchandise',
              place: 'Cholet'
            },
            {
              name: 'Enlèvement',
              icon: 'vehicule',
              place: 'Nantes',
            },
            {
              name: 'Livraison',
              icon: 'location',
              place: 'Toulouse',
            },
          ]}
        />
      ~~~

    `)(() => (
      <div style={{ marginLeft: '50px' }}>
        <Stepper
          currentStep={1}
          spaced={45}
          steps={[
            {
              name: 'Départ',
              icon: 'marchandise',
              place: 'Cholet',
            },
            {
              name: 'Enlèvement',
              icon: 'vehicule',
              place: 'Nantes',
            },
            {
              name: 'Livraison',
              icon: 'location',
              place: 'Toulouse',
            },
          ]}
        />
      </div>
    )),
  )
