import React from 'react'
import { connect } from 'react-fela'
import ds from 'views/styles/designSystem'
import { pxTo } from 'design-system-utils'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'

const Stepper = ({ styles, steps, currentStep, ...props }) => (
  <div className={styles.wrapper}>
  </div>
)

const baseFontSize = ds.get('type.baseFontSize')

const rules = {
  mobile: () => ({
    fontSize: pxTo(20, baseFontSize, 'rem'),
    textAlign: 'left',
  }),

  wrapper: () => ({
    display: 'flex',
    flexDirection: 'column-reverse',
  }),

  iconsWrapper: () => ({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline',
    minHeight: pxTo(50, baseFontSize, 'rem'),
  }),

  icon: (props) => ({
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'center',
    marginBottom: 'auto',
    height: '100%',
    minHeight: pxTo(50, baseFontSize, 'rem'),
    width: `${(1 / props.steps.length) * 100}%`,
    position: 'relative',
    zIndex: ds.get('zIndex.higher'),
    marginTop: pxTo(15, baseFontSize, 'rem'),

    ':first-child': {
      transform: 'translateX(-50%)',
      marginLeft: props.spaced !== 0 ? pxTo(props.spaced, baseFontSize, 'rem') : 0,
    },
    ':last-child': {
      transform: 'translateX(50%)',
      marginRight: props.spaced !== 0 ? pxTo(props.spaced, baseFontSize, 'rem') : 0,
    },
    '::before': {
      position: 'absolute',
      left: '50%',
      transform: 'translateX(-50%)',
      top: pxTo(-29, baseFontSize, 'rem'),
      content: '" "',
      height: pxTo(20, baseFontSize, 'rem'),
      width: pxTo(20, baseFontSize, 'rem'),
      backgroundColor: ds.brand('white'),
      borderColor: 'currentColor',
      borderStyle: 'solid',
      borderWidth: pxTo(1, baseFontSize, 'rem'),
      borderRadius: '100%',
    },
    '::after': {
      position: 'absolute',
      left: '50%',
      transform: 'translateX(-50%)',
      top: pxTo(-21, baseFontSize, 'rem'),
      content: '" "',
      height: pxTo(7, baseFontSize, 'rem'),
      width: pxTo(7, baseFontSize, 'rem'),
      backgroundColor: 'currentColor',
      borderRadius: '100%',
    },
  }),

  stepName: () => ({
    fontWeight: ds.get('type.fontWeight.bold'),
    fontSize: pxTo(ds.get('type.sizes.stepper.steps'), baseFontSize, 'rem'),
    marginTop: 'auto',
    whiteSpace: 'nowrap',
  }),
  placeName: () => ({
    fontSize: pxTo(ds.get('type.sizes.stepper.steps'), baseFontSize, 'rem'),
    marginTop: pxTo(5, baseFontSize, 'rem'),
  }),
  progress: (props) => ({
    height: pxTo(5, baseFontSize, 'rem'),
    width: '100%',
    position: 'relative',
    backgroundColor:
      props.currentStep < props.steps.length
        ? ds.get('colors.backgrounds.progress.todo')
        : ds.get('colors.backgrounds.progress.done'),
    '::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      height: 'inherit',
      width: `${(props.currentStep / props.steps.length) * 100}%`,
      backgroundImage: props.currentStep < props.steps.length ? ds.get('colors.backgrounds.progress.doing') : '',
      content: '" "',
    },
  }),
}

Stepper.propTypes = {
  styles: PropTypes.object.isRequired,
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      icon: PropTypes.string.isRequired,
      place: PropTypes.string,
    }),
  ).isRequired,
  currentStep: PropTypes.number,
  spaced: PropTypes.number,
  intl: PropTypes.object.isRequired,
}

Stepper.defaultProps = {
  spaced: 0,
}

export default injectIntl(connect(rules)(Stepper))
