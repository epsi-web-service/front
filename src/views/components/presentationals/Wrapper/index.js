import React from 'react'
import { createComponentWithProxy } from 'react-fela'
import PropTypes from 'prop-types'
import ds from 'views/styles/designSystem'
import { pxTo } from 'design-system-utils'

const Wrapper = ({ className, children }) => <div className={className}>{children}</div>

Wrapper.propTypes = {
  className: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element]).isRequired,
  titleAlign: PropTypes.string,
}

Wrapper.defaultProps = {
  titleAlign: 'center',
}

const documentBaseFontSize = ds.get('type.baseFontSize')
const defaultTextSize = ds.get('type.sizes.buttons.default')

const rule = ({ titleAlign }) => ({
  backgroundColor: ds.brand('white'),
  color: ds.get('colors.texts.default'),
  marginTop: pxTo(40, defaultTextSize, 'rem'),
  marginBottom: pxTo(40, defaultTextSize, 'rem'),
  paddingTop: pxTo(40, defaultTextSize, 'rem'),
  paddingBottom: pxTo(60, defaultTextSize, 'rem'),
  textAlign: 'left',
  '@media (min-width: 991px)': {
    paddingRight: pxTo(30, defaultTextSize, 'rem'),
    paddingLeft: pxTo(30, defaultTextSize, 'rem'),
  },
  '@media (min-width: 1280px)': {
    paddingRight: pxTo(60, defaultTextSize, 'rem'),
    paddingLeft: pxTo(60, defaultTextSize, 'rem'),
  },
  '> p': {
    marginTop: 0,
    marginBottom: pxTo(15, defaultTextSize, 'rem'),
    fontSize: pxTo(defaultTextSize, documentBaseFontSize, 'rem'),
    color: ds.brand('magneticBlack'),
  },
  '> h1': {
    textAlign: titleAlign,
    marginTop: 0,
    marginBottom: pxTo(15, defaultTextSize, 'rem'),
    color: ds.brand('magneticBlack'),
    display: 'block',
  },
  '> h2': {
    textAlign: titleAlign,
    marginTop: pxTo(10, defaultTextSize, 'rem'),
    marginBottom: pxTo(20, defaultTextSize, 'rem'),
    display: 'block',
    fontWeight: ds.get('type.fontWeight.normal'),
    fontSize: pxTo(ds.get('type.sizes.headings.level2'), documentBaseFontSize, 'rem'),
  },
})

export default createComponentWithProxy(rule, Wrapper)
