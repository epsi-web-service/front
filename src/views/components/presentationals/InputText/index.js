import React from 'react'
import PropTypes from 'prop-types'
const InputText = ({
  input,
  type,
  label,
  meta: { touched, error, warning },
  className,
  children,
  disabled,
  ...props
}) => (
  <div className={`${className} ${touched && error ? 'has-errors' : ''}`}>
    <div className="wrapper">
      <input {...props} type={type} disabled={disabled} {...input} id={input.name} />
      <label htmlFor={input.name} className={!input.value ? 'empty' : ''}>
        {label}
      </label>
      {children}
    </div>
    {touched &&
      ((error && <span className="error">{error}</span>) || (warning && <span className="error">{warning}</span>))}
  </div>
)

InputText.defaultProps = {
  type: 'text',
  meta: {
    touched: false,
    error: false,
    warning: false,
  },
}

InputText.propTypes = {
  label: PropTypes.string.isRequired,
  input: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }),
  type: PropTypes.string,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.any,
    warning: PropTypes.any,
  }),
  children: PropTypes.element,
}

export default InputText
