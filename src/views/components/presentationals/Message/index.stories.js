import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react' // eslint-disable-line no-unused-vars

// add-on
import { withInfo } from '@storybook/addon-info'

import { text, select, boolean } from '@storybook/addon-knobs'

// component

import Message from './'

const brands = ['error', 'info']

storiesOf('Message', module)
  .add(
    'Usage',
    withInfo(`
      #### Import
      ~~~js
      import Message from 'views/components/presentationals/Message'
      ~~~
      ---
      #### Usage
      ~~~js
      <Message brand="brand" outline={false|true} withBorderRadius={false|true}>
        Hey
      </Message>
      ~~~

    `)(() => (
      <Message
        brand={select('Brand', brands, 'info', 'exampleMessageBrandsList')}
        withBorderRadius={boolean('With border radius', false)}
        outline={boolean('Outline', false)}
      >
        <Fragment>{text('label', 'Hey')}</Fragment>
      </Message>
    )),
  )
  .add(
    'Themes',
    withInfo(`
      #### Import
      ~~~js
      import Message from 'views/components/presentationals/Message'
      ~~~
      ---
      #### Usage
      ~~~js
      <Message brand="error" ... >
        Hey
      </Message>

      <Message brand="info" ... >
        Hey
      </Message>
      ~~~

    `)(() => (
      <div>
        {brands.map((brand, key) => (
          <div key={key} style={{ margin: '15px' }}>
            <Message brand={brand}>
              <Fragment>{brand} message</Fragment>
            </Message>
          </div>
        ))}
      </div>
    )),
  )
  .add(
    'Outline',
    withInfo(`
      #### Import
      ~~~js
      import Message from 'views/components/presentationals/Message'
      ~~~
      ---
      #### Usage
      ~~~js
      <Message outline={true} ...>
        Hey
      </Message>
      ~~~

    `)(() => (
      <Message brand={select('Brand', brands, 'info', 'exampleMessageBrandsList')} outline={true}>
        Hey
      </Message>
    )),
  )
  .add(
    'With border radius',
    withInfo(`
      #### Import
      ~~~js
      import Message from 'views/components/presentationals/Message'
      ~~~
      ---
      #### Usage
      ~~~js
      <Message withBorderRadius={true} ...>
        Hey
      </Message>
      ~~~

    `)(() => (
      <Message
        brand={select('Brand', brands, 'info', 'exampleMessageBrandsList')}
        outline={boolean('Outline', false)}
        withBorderRadius={true}
      >
        Hey
      </Message>
    )),
  )
