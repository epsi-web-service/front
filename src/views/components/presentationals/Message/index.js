import React from 'react'
import { connect } from 'react-fela'
import ds from 'views/styles/designSystem'
import { pxTo } from 'design-system-utils'
import PropTypes from 'prop-types'

const Message = ({ styles, children, brand, outline, withBorderRadius, margin, ...props }) => (
  <div {...props} className={[styles.wrapper, props.className].join(' ')}>
    {children}
  </div>
)

const baseFontSize = ds.get('type.baseFontSize')
const backgrounds = {
  error: ds.brand('brightOrange'),
  info: ds.brand('skyBlue'),
  transparent: 'transparent',
}

const colors = {
  error: ds.brand('white'),
  info: ds.brand('white'),
  transparent: ds.brand('white'),
}

const borders = {
  error: ds.brand('brightOrange'),
  info: ds.brand('skyBlue'),
  transparent: ds.brand('greyMedium'),
}

const rules = {
  wrapper: ({ brand, outline, withBorderRadius, margin }) => ({
    width: '100%',
    background: outline === true ? 'transparent' : backgrounds[brand],
    color: outline === true ? backgrounds[brand] : colors[brand],
    padding: `${pxTo(15, baseFontSize, 'rem')} ${pxTo(20, baseFontSize, 'rem')}`,
    borderRadius: withBorderRadius === true ? pxTo(ds.get('border.radius.messages'), baseFontSize, 'rem') : 0,
    borderColor: outline === true ? borders[brand] : 'transparent',
    borderTopColor: outline === 'top' ? borders[brand] : 'transparent',
    borderBottomColor: outline === 'bottom' ? borders[brand] : 'transparent',
    borderLeftColor: outline === 'left' ? borders[brand] : 'transparent',
    borderRightColor: outline === 'right' ? borders[brand] : 'transparent',
    borderWidth: outline === false ? 0 : pxTo(1, baseFontSize, 'rem'),
    borderStyle: 'solid',
    marginBottom: pxTo(margin, baseFontSize, 'rem'),
  }),
}

Message.defaultProps = {
  brand: 'info',
  outline: false,
  withBorderRadius: false,
  margin: 0,
}

Message.propTypes = {
  styles: PropTypes.object.isRequired,
  brand: PropTypes.oneOf(['error', 'info', 'transparent']).isRequired,
  outline: PropTypes.oneOfType([PropTypes.bool, PropTypes.oneOf(['top', 'bottom', 'right', 'left'])]).isRequired,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element), PropTypes.string]).isRequired,
  withBorderRadius: PropTypes.bool.isRequired,
  margin: PropTypes.number.isRequired,
}

export default connect(rules)(Message)
