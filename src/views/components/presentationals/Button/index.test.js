import React from 'react'
import { mount } from 'enzyme'
import { felaSnapshot, felaTestContext } from '@cloudflare/style-provider'
import Button from './'

test('renders', () => {
  const snapshot = felaSnapshot(<Button />)
  expect(snapshot.component).toMatchSnapshot()
  expect(snapshot.styles).toMatchSnapshot()
})

test('renders with text', () => {
  const snapshot = felaSnapshot(<Button>Hello wordy button</Button>)
  expect(snapshot.component).toMatchSnapshot()
  expect(snapshot.styles).toMatchSnapshot()
})

test('renders with dynamic styles', () => {
  const snapshot = felaSnapshot(<Button primary>Haiii</Button>)
  expect(snapshot.component).toMatchSnapshot()
  expect(snapshot.styles).toMatchSnapshot()
})

test('reacts to onClick', () => {
  const handleClick = jest.fn()
  const wrapper = mount(felaTestContext(<Button onClick={handleClick}>Hit me</Button>))
  expect(handleClick.mock.calls.length).toBe(0)
  wrapper.find(Button).simulate('click')
  expect(handleClick.mock.calls.length).toBe(1)
})
