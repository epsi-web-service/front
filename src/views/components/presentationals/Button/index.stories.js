import React from 'react'
import { storiesOf } from '@storybook/react' // eslint-disable-line no-unused-vars

// add-on
import { withInfo } from '@storybook/addon-info'

import { action } from '@storybook/addon-actions'
import { text, select, boolean } from '@storybook/addon-knobs'

// component
import Button from './'

const brands = ['primary', 'secondary', 'tertiary', 'quaternary']
const sizes = ['xs', 'sm', 'md', 'lg']

storiesOf('Button', module)
  .add(
    'Usage',
    withInfo(`
      #### Import
      ~~~js
      import Button from 'views/components/presentationals/Button'
      ~~~
      ---
      #### Usage
      ~~~js
      <Button disabled={true|false} brand="brand" size="size" outline={false|true} reversed={false|true} onClick={onClick}>
        Hey
      </Button>
      ~~~

    `)(() => (
      <Button
        disabled={boolean('Disabled', false)}
        brand={select('Brand', brands, 'primary', 'exampleButtonBrandsList')}
        size={select('Size', sizes, 'md', 'exampleButtonSizesList')}
        onClick={action(console.log('clicked'))}
      >
        {text('label', 'Hey')}
      </Button>
    )),
  )
  .add(
    'Themes',
    withInfo(`
      #### Import
      ~~~js
      import Button from 'views/components/presentationals/Button'
      ~~~
      ---
      #### Usage
      ~~~js
      <Button brand="primary" ...>
        Hey
      </Button>

      <Button brand="secondary" ...>
        Hey
      </Button>

      <Button brand="tertiary" ...>
        Hey
      </Button>

      <Button brand="quaternary" ...>
        Hey
      </Button>
      ~~~

    `)(() => (
      <div style={{ display: 'flex' }}>
        {brands.map((brand, key) => (
          <div key={key} style={{ margin: '15px' }}>
            <Button
              brand={brand}
              disabled={boolean('Disabled', false)}
              size={select('Size', sizes, 'md', 'brandedButtonListSizes')}
              onClick={action(console.log('clicked'))}
              outline={boolean('Outline', false)}
              reversed={boolean('Reversed', false)}
            >
              {brand} button
            </Button>
          </div>
        ))}
      </div>
    )),
  )
  .add(
    'Sizes',
    withInfo(`
      #### Import
      ~~~js
      import Button from 'views/components/presentationals/Button'
      ~~~
      ---
      #### Usage
      ~~~js
      <Button size="xs" ...>
        xs button
      </Button>

      <Button size="sm" ...>
        sm button
      </Button>

      <Button size="md" ...>
        md button
      </Button>

      <Button size="lg" ...>
        lg button
      </Button>
      ~~~

    `)(() => (
      <div style={{ display: 'flex' }}>
        {sizes.map((size, key) => (
          <div key={key} style={{ margin: '15px' }}>
            <Button
              disabled={boolean('Disabled', false)}
              brand={select('Brand', brands, 'primary', 'sizedButtonListBrands')}
              size={size}
              onClick={action(console.log('clicked'))}
            >
              {size} button
            </Button>
          </div>
        ))}
      </div>
    )),
  )
  .add(
    'Reversed',
    withInfo(`
      #### Import
      ~~~js
      import Button from 'views/components/presentationals/Button'
      ~~~
      ---
      #### Usage
      ~~~js

      <Button reversed={true} ...>
        Reversed button
      </Button>
      ~~~

    `)(() => (
      <div style={{ display: 'flex' }}>
        <div style={{ margin: '15px' }}>
          <Button
            brand={select('Brand', brands, 'primary', 'enabledButtonListBrands')}
            size={select('Size', sizes, 'md', 'enabledButtonListSizes')}
            onClick={action(console.log('clicked'))}
            reversed={true}
          >
            Reversed button
          </Button>
        </div>
      </div>
    )),
  )
  .add(
    'Outline',
    withInfo(`
      #### Import
      ~~~js
      import Button from 'views/components/presentationals/Button'
      ~~~
      ---
      #### Usage
      ~~~js

      <Button reversed={true} ...>
        Outline button
      </Button>
      ~~~

    `)(() => (
      <div style={{ display: 'flex' }}>
        <div style={{ margin: '15px' }}>
          <Button
            brand={select('Brand', brands, 'primary', 'enabledButtonListBrands')}
            size={select('Size', sizes, 'md', 'enabledButtonListSizes')}
            onClick={action(console.log('clicked'))}
            outline={true}
          >
            Outline button
          </Button>
        </div>
      </div>
    )),
  )
  .add(
    'Disabled',
    withInfo(`
      #### Import
      ~~~js
      import Button from 'views/components/presentationals/Button'
      ~~~
      ---
      #### Usage
      ~~~js

      <Button disabled={false} ...>
        Enabled
      </Button>

      <Button disabled={true}  ...>
        Disabled
      </Button>

      ~~~

    `)(() => (
      <div style={{ display: 'flex' }}>
        <div style={{ margin: '15px' }}>
          <Button
            disabled={false}
            brand={select('Brand', brands, 'primary', 'enabledButtonListBrands')}
            size={select('Size', sizes, 'md', 'enabledButtonListSizes')}
            onClick={action(console.log('clicked'))}
          >
            Enable button
          </Button>
        </div>
        <div style={{ margin: '15px' }}>
          <Button
            disabled={true}
            brand={select('Brand', brands, 'primary', 'enabledButtonListBrands')}
            size={select('Size', sizes, 'md', 'enabledButtonListSizes')}
            onClick={action(console.log('clicked'))}
          >
            Disabled button
          </Button>
        </div>
      </div>
    )),
  )
