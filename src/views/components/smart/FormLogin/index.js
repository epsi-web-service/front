import { reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { loginRequest } from 'store/ducks/authentication'
import FormLogin from './presentational'

const mapStateToProps = (state) => ({
  loading: state.authentication.loading,
  errors: state.authentication.errors,
})

const mapDispatchToProps = (dispatch) => ({
  onSubmit: (payload) => dispatch(loginRequest(payload.username, payload.password)),
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(FormLogin)
const formed = reduxForm({ form: 'login' })(connected)

export default formed
