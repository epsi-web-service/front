import React, { PureComponent } from 'react'
import { Field } from 'redux-form'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { injectIntl, FormattedMessage } from 'react-intl'
import { createComponentWithProxy } from 'react-fela'
import ds from 'views/styles/designSystem'
import { pxTo } from 'design-system-utils'
import { required, length } from 'redux-form-validators'
import phoneOrEmail from 'utils/validators/phoneOrEmail'
import InputText from 'views/components/presentationals/InputText'
import Button from 'views/components/presentationals/Button'
import Message from 'views/components/presentationals/Message'

class FormLogin extends PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    onSubmit: PropTypes.func.isRequired,
    loading: PropTypes.bool,
    errors: PropTypes.array.isRequired,
    intl: PropTypes.object.isRequired,
    className: PropTypes.string.isRequired,
  }

  render() {
    const { handleSubmit, onSubmit, loading, errors, intl, className } = this.props

    return (
      <div className={className}>
        {errors.length > 0 && (
          <div data-selector="components.smart.formLogin.error">
            {errors.map((error, key) => (
              <Message key={key} brand="error" withBorderRadius={true} margin={20}>
                {error}
              </Message>
            ))}
          </div>
        )}

        <form onSubmit={handleSubmit(onSubmit)}>
          <Field
            name="username"
            component={InputText}
            type="text"
            label={intl.formatMessage({ id: 'components.smart.formLogin.username' })}
            validate={[required(), length({ minimum: 3 }), phoneOrEmail()]}
          />
          <Field
            name="password"
            component={InputText}
            type="password"
            label={intl.formatMessage({ id: 'components.smart.formLogin.password' })}
            validate={[required(), length({ minimum: 3 })]}
          />
          <Link to="/forgot" className="forgot">
            <FormattedMessage id="components.smart.formLogin.forgot" />
          </Link>

          <Button
            brand="primary"
            size="md"
            block
            className="btn-login"
            disabled={loading || process.env.NODE_ENV === 'production'}
          >
            <FormattedMessage
              id={loading ? 'components.smart.formLogin.loading' : 'components.smart.formLogin.login'}
            />
          </Button>
        </form>
      </div>
    )
  }
}

const documentBaseFontSize = ds.get('type.baseFontSize')
const xsTextSize = ds.get('type.sizes.links.xs')
const defaultTextSize = ds.get('type.sizes.links.default')

const rule = () => ({
  '& .forgot': {
    textAlign: 'right',
    display: 'block',
    fontSize: pxTo(xsTextSize, documentBaseFontSize, 'rem'),
    color: ds.brand('blueNight'),
  },
  '& .btn-login': {
    marginTop: pxTo(30, defaultTextSize, 'em'),
  },
})

export default injectIntl(createComponentWithProxy(rule, FormLogin))
