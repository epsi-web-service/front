import { connect } from 'react-redux'
import { registerRequest } from 'store/ducks/register'
import FormRegister from './presentational'

const mapStateToProps = (state) => ({
  created: state.register.signup.created,
  entity: state.register.signup.entity,
  violations: state.register.signup.violations,
})

const mapDispatchToProps = (dispatch) => ({
  onSubmit: (values) => dispatch(registerRequest(values)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FormRegister)
