import React, { Fragment, PureComponent } from 'react'
import Stepper from 'views/components/presentationals/Stepper'
import ds from 'views/styles/designSystem'
import { pxTo } from 'design-system-utils'
import loadable from 'loadable-components'
import { injectIntl } from 'react-intl'
import { connect } from 'react-fela'
import { Row, Col } from 'react-flexbox-grid'
import PropTypes from 'prop-types'
import Message from 'views/components/presentationals/Message'

class FormRegister extends PureComponent {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    created: PropTypes.bool.isRequired,
    violations: PropTypes.array.isRequired,
    entity: PropTypes.object,
    intl: PropTypes.object.isRequired,
    styles: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this.state = {
      step: 1,
    }

    this.goToNextStep = this.goToNextStep.bind(this)
    this.goToPreviousStep = this.goToPreviousStep.bind(this)
  }

  goToNextStep() {
    this.setState({ step: this.state.step + 1 })
  }

  goToPreviousStep() {
    this.setState({ step: this.state.step - 1 })
  }

  render() {
    const { onSubmit, violations, created, entity, intl, styles } = this.props
    return (
        <Row center="xs">
        </Row>
    )
  }
}

const rules = {
  stepperWrapper: () => ({
    marginBottom: pxTo(40, ds.get('type.baseFontSize'), 'rem'),
  }),
  messages: () => ({
    margin: `0 0 ${pxTo(40, ds.get('type.baseFontSize'), 'rem')}`,
  }),
  message: () => ({
    margin: `0 0 ${pxTo(12, ds.get('type.baseFontSize'), 'rem')}`,
  }),
}

export default injectIntl(connect(rules)(FormRegister))
