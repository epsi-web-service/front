import { reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import FormLogin from './presentational'

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = (dispatch) => ({
})

const formed = reduxForm({ form: 'company', enableReinitialize: true })(FormLogin)
const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(formed)

export default connected
