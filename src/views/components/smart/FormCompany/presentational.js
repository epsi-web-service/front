import React, { PureComponent } from 'react'
import { Field } from 'redux-form'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid'
import ds from 'views/styles/designSystem'
import { pxTo } from 'design-system-utils'
import { injectIntl, FormattedMessage } from 'react-intl'
import { connect } from 'react-fela'
import { required, length } from 'redux-form-validators'
import postalCode from 'utils/validators/postalCode'
import InputText from 'views/components/presentationals/InputText'
import Button from 'views/components/presentationals/Button'

class FormVehicle extends PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    loading: PropTypes.bool.isRequired,
    violations: PropTypes.array.isRequired,
    intl: PropTypes.object.isRequired,
    updateRequest: PropTypes.func.isRequired,
    initFormData: PropTypes.func.isRequired,
    resetFormData: PropTypes.func.isRequired,
    initialValues: PropTypes.object.isRequired,
    styles: PropTypes.object.isRequired,
    profile: PropTypes.object,
  }

  constructor(props) {
    super(props)

    this.state = {
      modalIsOpen: false,
    }

    this.handleOnSubmit = this.handleOnSubmit.bind(this)
  }

  handleOnSubmit(values) {
    this.props.updateRequest(values)
  }

  componentDidMount() {
    if (this.props.profile && this.props.profile.managedCompany) {
      this.props.initFormData(this.props.profile.managedCompany)
    }
  }

  componentDidUpdate() {
    if (this.props.profile && this.props.profile.managedCompany) {
      this.props.initFormData(this.props.profile.managedCompany)
    }
  }

  componentWillUnmount() {
    this.props.resetFormData()
  }

  render() {
    const { handleSubmit, styles, loading, violations, intl, profile } = this.props

    return (
      <div>
        {loading && <div data-selector="components.smart.formCompany.loading">Loading</div>}

        {violations.length > 0 && (
          <div>
            <ul>
              {violations.map((violation) => (
                <li key={violation.propertyPath}>{violation.message}</li>
              ))}
            </ul>
          </div>
        )}

        {!profile.managedCompany && <div data-selector="components.smart.formCompany.loading">Loading</div>}

        {!!profile.managedCompany && (
          <form onSubmit={handleSubmit(this.handleOnSubmit)}>
            <Row>
              <Col xs={6}>
                <Field
                  name="name"
                  component={InputText}
                  type="text"
                  label={intl.formatMessage({
                    id: 'components.smart.formCompany.corporateName',
                  })}
                  validate={[required(), length({ minimum: 3 })]}
                />
              </Col>
              <Col xs={6}>
                <Field
                  name="vatIdentificationNumber"
                  component={InputText}
                  type="text"
                  label={intl.formatMessage({
                    id: 'components.smart.formCompany.vatIdentificationNumber',
                  })}
                  validate={[required(), length({ minimum: 1 })]}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <Field
                  name="streetAddress"
                  component={InputText}
                  type="text"
                  label={intl.formatMessage({
                    id: 'components.smart.formCompany.streetAddress',
                  })}
                  validate={[required(), length({ minimum: 3 })]}
                />
              </Col>
              <Col xs={6}>
                <Field
                  name="additionalAddress"
                  component={InputText}
                  type="text"
                  label={intl.formatMessage({
                    id: 'components.smart.formCompany.additionalAddress',
                  })}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <Field
                  name="postalCode"
                  component={InputText}
                  type="text"
                  label={intl.formatMessage({
                    id: 'components.smart.formCompany.postalCode',
                  })}
                  validate={[required(), postalCode()]}
                />
              </Col>
              <Col xs={6}>
                <Field
                  name="city"
                  component={InputText}
                  type="text"
                  label={intl.formatMessage({
                    id: 'components.smart.formCompany.city',
                  })}
                  validate={[length({ minimum: 1 })]}
                />
              </Col>
            </Row>

            <Row>
              <Col xs={6}>
                <Field
                  name="country"
                  component={InputText}
                  type="text"
                  label={intl.formatMessage({
                    id: 'components.smart.formCompany.country',
                  })}
                  validate={[length({ minimum: 1 })]}
                />
              </Col>
            </Row>

            <Row center="xs" className={styles.actions}>
              <Col xs={6}>
                <Button brand="primary" size="md" disabled={loading}>
                  <FormattedMessage id="components.smart.formCompany.button.update" />
                </Button>
              </Col>
            </Row>
          </form>
        )}
      </div>
    )
  }
}

const baseFontSize = ds.get('type.baseFontSize')

const rules = {
  title: () => ({
    textAlign: 'center',
  }),
  actions: () => ({
    marginTop: pxTo(30, baseFontSize, 'rem'),
  }),
  modalTitle: () => ({
    textAlign: 'center',
  }),
  modalSubtitle: () => ({
    textAlign: 'center',
  }),
  buttonsWrapper: () => ({
    marginTop: pxTo(25, ds.get('type.baseFontSize'), 'rem'),
  }),
}

export default injectIntl(connect(rules)(FormVehicle))
