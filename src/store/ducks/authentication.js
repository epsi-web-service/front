import { combineEpics } from 'redux-observable'
import { Observable } from 'rxjs/Rx'
import { push } from 'react-router-redux'
import jwtDecode from 'jwt-decode'
import { login, refresh } from 'store/services/authentication'
import * as appDuck from 'store/ducks/app'

export const LOCALSTORAGE_REFRESH_TOKEN = 'refresh_token'

// Actions
export const LOGIN_REQUEST = 'authentication/LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'authentication/LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'authentication/LOGIN_FAILURE'
export const REFRESH_TOKEN_REQUEST = 'authentication/REFRESH_TOKEN_REQUEST'
export const REFRESH_TOKEN_SUCCESS = 'authentication/REFRESH_TOKEN_SUCCESS'
export const REFRESH_TOKEN_FAILURE = 'authentication/REFRESH_TOKEN_FAILURE'
export const LOGOUT_REQUEST = 'authentication/LOGOUT_REQUEST'
export const LOGOUT_SUCCESS = 'authentication/LOGOUT_SUCCESS'

// Reducer
const INITIAL_STATE = {
  token: null,
  payload: {},
  authenticated: false,
  ready: false,
  loading: false,
  errors: [],
}

export function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        token: null,
        payload: {},
        authenticated: false,
        loading: true,
        errors: [],
      }

    case LOGIN_SUCCESS:
    case REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        token: action.token,
        payload: action.payload,
        authenticated: true,
        ready: true,
        loading: false,
        errors: [],
      }

    case LOGIN_FAILURE:
      return {
        ...state,
        errors: action.errors,
        loading: false,
      }

    case REFRESH_TOKEN_FAILURE:
      return {
        ...INITIAL_STATE,
        ready: true,
      }

      case LOGOUT_REQUEST:
      return INITIAL_STATE

    default:
      return state
  }
}

// Actions Creators
export const loginRequest = (username, password) => ({
  type: LOGIN_REQUEST,
  payload: {
    username,
    password,
  },
})

export const loginSuccess = (token, payload) => ({
  type: LOGIN_SUCCESS,
  token,
  payload,
})

export const loginFailure = (errors) => ({
  type: LOGIN_FAILURE,
  errors,
})

export const refreshTokenRequest = (action = null) => ({
  type: REFRESH_TOKEN_REQUEST,
  retry: action,
})

export const refreshTokenSuccess = (token, payload) => ({
  type: REFRESH_TOKEN_SUCCESS,
  token,
  payload,
})

export const refreshTokenFailure = () => ({
  type: REFRESH_TOKEN_FAILURE,
})

export const logoutRequest = () => ({
  type: LOGOUT_REQUEST,
})

export const logoutSuccess = () => ({
  type: LOGOUT_SUCCESS,
})

// Epics
export const epic = combineEpics(
  initAppEpic,
  loginRequestEpic,
  loginSuccessEpic,
  refreshTokenRequestEpic,
  logoutRequestEpic,
  refreshTokenSuccessEpic
)

export function initAppEpic(action$) {
  return action$.ofType(appDuck.INIT).mapTo(refreshTokenRequest())
}

export function loginRequestEpic(action$, store) {
  return action$.ofType(LOGIN_REQUEST).mergeMap((action) =>
    Observable.ajax(login({ payload: action.payload }))
      .mergeMap((res) => {
        localStorage.setItem(LOCALSTORAGE_REFRESH_TOKEN, res.response.refresh_token)
        return Observable.of(
          loginSuccess(res.response.token, jwtDecode(res.response.token)),
        )
      })
      .catch(() => {
        let errors = 'Email ou mot de passe incorrect'
        return Observable.of(loginFailure(errors))
      }),
  )
}

export function loginSuccessEpic(action$) {
  return action$.ofType(LOGIN_SUCCESS).map(() => push('/todolist'))
}

export function refreshTokenRequestEpic(action$) {
  return action$.ofType(REFRESH_TOKEN_REQUEST).mergeMap((action) => {
    const token = localStorage.getItem(LOCALSTORAGE_REFRESH_TOKEN)

    if (!token) {
      return Observable.of(refreshTokenFailure())
    } else {
      return Observable.ajax(refresh({ params: { token } }))
        .mergeMap((res) => {
          localStorage.setItem(LOCALSTORAGE_REFRESH_TOKEN, res.response.refresh_token)
          let actions = [refreshTokenSuccess(res.response.token, jwtDecode(res.response.token))]

          if (action.retry) {
            actions.push(action.retry)
          }

          return Observable.from(actions)
        })
        .catch(() => Observable.of(refreshTokenFailure()))
    }
  })
}

export function logoutRequestEpic(action$) {
  return action$.ofType(LOGOUT_REQUEST).map((action) => {
    return logoutSuccess()
  })
}

export function refreshTokenSuccessEpic(action$) {
  return action$.ofType(REFRESH_TOKEN_SUCCESS).map(() => push('/todolist'))
}
