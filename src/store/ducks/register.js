import { combineEpics } from 'redux-observable'
import { Observable } from 'rxjs/Rx'
import { register } from 'store/services/register'
import { loginRequest } from 'store/ducks/authentication'

// Actions
export const REGISTER_REQUEST = 'register/REGISTER_REQUEST'
export const REGISTER_SUCCESS = 'register/REGISTER_SUCCESS'
export const REGISTER_FAILURE = 'register/REGISTER_FAILURE'

// Reducer
const INITIAL_STATE = {
    signup: {
        loading: false,
        created: false,
        violations: [],
        entity: {},
    },
}

export function reducer(state = INITIAL_STATE, action = {}) {
    switch (action.type) {
        case REGISTER_REQUEST:
            return {
                ...state,
                signup: {
                    loading: true,
                    created: false,
                    violations: [],
                    entity: {},
                },
            }

        case REGISTER_SUCCESS:
            return {
                ...state,
                signup: {
                    loading: false,
                    created: true,
                    violations: [],
                    entity: action.entity,
                },
            }

        case REGISTER_FAILURE:
            return {
                ...state,
                signup: {
                    loading: false,
                    created: false,
                    violations: action.violations,
                    entity: {},
                },
            }
        default:
            return state
    }
}

// Actions Creators
export const registerRequest = (raw) => ({
    type: REGISTER_REQUEST,
    payload: {
        email: raw.email,
        plainPassword: raw.password,
        username: raw.email
    },
})

export const registerSuccess = (entity, credentials) => ({
    type: REGISTER_SUCCESS,
    entity,
    credentials: {
        username: credentials.email,
        password: credentials.plainPassword
    }
})

export const registerFailure = (violations) => ({
    type: REGISTER_FAILURE,
    violations,
})

// Epics
export const epic = combineEpics(
    registerRequestEpic,
    sendLoginOnRegisterSuccessEpic,
)

export function registerRequestEpic(action$) {
    return action$.ofType(REGISTER_REQUEST).mergeMap((action) =>
        Observable.ajax(register({ payload: action.payload }))
            .map((res) => registerSuccess(res.response, action.payload))
            .catch((res) => Observable.of(registerFailure("Erreur lors de l'inscription")))
            .catch((res) => Observable.of(registerFailure("Erreur lors de l'inscription"))),
    )
}


export function sendLoginOnRegisterSuccessEpic(action$) {
    return action$.ofType(REGISTER_SUCCESS).mergeMap((action) => {
        return Observable.of(loginRequest(action.credentials.username, action.credentials.password))
    })
}
