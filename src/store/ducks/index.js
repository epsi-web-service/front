import { combineReducers } from 'redux'
import { combineEpics } from 'redux-observable'
import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form'
import { reducer as authenticationReducer, epic as authenticationEpic } from 'store/ducks/authentication'
import { reducer as registerReducer, epic as registerEpic } from 'store/ducks/register'
import { reducer as todolistReducer, epic as todolistEpic } from 'store/ducks/todolist'

export const reducer = combineReducers({
  authentication: authenticationReducer,
  register: registerReducer,
  form: formReducer,
  router: routerReducer,
  todolist: todolistReducer
})

export const epic = combineEpics(
  authenticationEpic,
  registerEpic,
  todolistEpic
)
