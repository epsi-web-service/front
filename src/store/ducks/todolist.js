import { combineEpics } from 'redux-observable'
import { Observable } from 'rxjs/Rx'
import {LOGIN_SUCCESS, REFRESH_TOKEN_SUCCESS} from "store/ducks/authentication";
import { fetchAllTodo, postTodo, patchTodoToggle, deleteTodo, patchTodoDescription } from 'store/services/todolist'

// Actions
export const FETCH_ALL_TODO_REQUEST = 'todolist/FETCH_ALL_TODO_REQUEST'
export const FETCH_ALL_TODO_SUCESS = 'todolist/FETCH_ALL_TODO_SUCESS'
export const FETCH_ALL_TODO_FAILURE = 'todolist/FETCH_ALL_TODO_FAILURE'

export const POST_TODO_REQUEST = 'todolist/POST_TODO_REQUEST'
export const POST_TODO_SUCESS = 'todolist/POST_TODO_SUCESS'
export const POST_TODO_FAILURE = 'todolist/POST_TODO_FAILURE'


export const PATCH_TODO_TOGGLE_REQUEST = 'todolist/PATCH_TODO_TOGGLE_REQUEST'
export const PATCH_TODO_TOGGLE_SUCESS = 'todolist/PATCH_TODO_TOGGLE_SUCESS'
export const PATCH_TODO_TOGGLE_FAILURE = 'todolist/PATCH_TODO_TOGGLE_FAILURE'

export const PATCH_TODO_DESCRIPTION_REQUEST = 'todolist/PATCH_TODO_DESCRIPTION_REQUEST'
export const PATCH_TODO_DESCRIPTION_SUCESS = 'todolist/PATCH_TODO_DESCRIPTION_SUCESS'
export const PATCH_TODO_DESCRIPTION_FAILURE = 'todolist/PATCH_TODO_DESCRIPTION_FAILURE'


export const DELETE_TODO_REQUEST = 'todolist/DELETE_TODO_REQUEST'
export const DELETE_TODO_SUCESS = 'todolist/DELETE_TODO_SUCESS'
export const DELETE_TODO_FAILURE = 'todolist/DELETE_TODO_FAILURE'

export const UPDATE_TODO_DESCRIPTION_LOCAL = 'todolist/UPDATE_TODO_DESCRIPTION_LOCAL'

export const RESET_TODO = 'todolist/RESET_TODO'


// Reducer
const INITIAL_STATE = {
    todos: [],
}

export function reducer(state = INITIAL_STATE, action = {}) {
    switch (action.type) {
        case FETCH_ALL_TODO_SUCESS:
            return {
                ...state,
                todos: state.todos.concat(action.payload)
            }
        case POST_TODO_SUCESS:
            return {
                ...state,
                todos: [...state.todos, action.payload]
            }
        case PATCH_TODO_DESCRIPTION_SUCESS:
        case PATCH_TODO_TOGGLE_SUCESS:
            return {
                ...state,
                todos: state.todos.map((todo) => {
                    if(todo.id === action.payload.id) {
                        return action.payload;
                    } else {
                        return todo;
                    }
                })
            }
        case DELETE_TODO_SUCESS:
            return {
                ...state,
                todos: state.todos.filter((todo) => todo.id !== action.payload.id)
            }
        case UPDATE_TODO_DESCRIPTION_LOCAL:
            return {
                ...state,
                todos: state.todos.map((todo) => {
                    if(todo.id === action.payload.id) {
                        todo.description = action.payload.value
                        return todo;
                    } else {
                        return todo;
                    }
                })
            }
        case RESET_TODO:
            return INITIAL_STATE;
        default:
            return state
    }
}


// Actions Creators
export const fetchAllTodoRequest = () => ({
    type: FETCH_ALL_TODO_REQUEST,
})

export const fetchAllTodoSuccess = (payload) => ({
    type: FETCH_ALL_TODO_SUCESS,
    payload
})

export const fetchAllTodoFailure = () => ({
    type: FETCH_ALL_TODO_FAILURE,
})


// Actions Creators
export const postTodoRequest = (name) => ({
    type: POST_TODO_REQUEST,
    todo: name
})

export const postTodoSuccess = (payload) => ({
    type: POST_TODO_SUCESS,
    payload
})

export const postTodoFailure = () => ({
    type: POST_TODO_FAILURE,
})

export const patchTodoToggleRequest = (id, value) => ({
    type: PATCH_TODO_TOGGLE_REQUEST,
    id,
    value
})

export const patchTodoToggleSuccess = (payload) => ({
    type: PATCH_TODO_TOGGLE_SUCESS,
    payload
})

export const patchTodoToggleFailure = () => ({
    type: PATCH_TODO_TOGGLE_FAILURE,
})

export const deleteTodoRequest = (id) => ({
    type: DELETE_TODO_REQUEST,
    id,
})

export const deleteTodoSuccess = (id) => ({
    type: DELETE_TODO_SUCESS,
    payload: {
        id
    }
})

export const deleteTodoFailure = () => ({
    type: DELETE_TODO_FAILURE,
})

export const patchTodoDescriptionRequest = (id) => ({
    type: PATCH_TODO_DESCRIPTION_REQUEST,
    id,
})

export const patchTodoDescriptionSuccess = (payload) => ({
    type: PATCH_TODO_DESCRIPTION_SUCESS,
    payload
})

export const patchTodoDescriptionFailure = () => ({
    type: PATCH_TODO_TOGGLE_FAILURE,
})

export const updateTodoDescriptionLocal = (id, value) => ({
    type: UPDATE_TODO_DESCRIPTION_LOCAL,
    payload:{
        id,
        value
    }
})

export const resetTodos = () => ({
    type: RESET_TODO,
})


// Epics
export const epic = combineEpics(
    fetchAllTodoOnLoginSuccessEpic,
    postTodoEpic,
    patchTodoToggleEpic,
    deleteTodoEpic,
    patchTodoDescriptionEpic
)

export function fetchAllTodoOnLoginSuccessEpic(action$, state$) {
    return action$.ofType(LOGIN_SUCCESS, REFRESH_TOKEN_SUCCESS).mergeMap((action) =>
        Observable.ajax(fetchAllTodo({ token: state$.getState().authentication.token }))
            .map((res) => fetchAllTodoSuccess(res.response))
            .catch((res) => fetchAllTodoFailure())
    )
}

export function postTodoEpic(action$, state$) {
    return action$.ofType(POST_TODO_REQUEST).mergeMap((action) =>
        Observable.ajax(postTodo({todo: action.todo, token: state$.getState().authentication.token }))
            .map((res) => postTodoSuccess(res.response))
            .catch((res) => postTodoFailure())
    )
}

export function patchTodoToggleEpic(action$, state$) {
    return action$.ofType(PATCH_TODO_TOGGLE_REQUEST).mergeMap((action) =>
        Observable.ajax(patchTodoToggle({id: action.id, token: state$.getState().authentication.token }))
            .map((res) => patchTodoToggleSuccess(res.response))
            .catch((res) => patchTodoToggleFailure())
    )
}


export function deleteTodoEpic(action$, state$) {
    return action$.ofType(DELETE_TODO_REQUEST).mergeMap((action) =>
        Observable.ajax(deleteTodo({id: action.id, token: state$.getState().authentication.token }))
            .map((res) => deleteTodoSuccess(action.id))
            .catch((res) => deleteTodoFailure())
    )
}

export function patchTodoDescriptionEpic(action$, state$) {
    return action$.ofType(PATCH_TODO_DESCRIPTION_REQUEST).mergeMap((action) =>
        Observable.ajax(patchTodoDescription({id: action.id, token: state$.getState().authentication.token, value: state$.getState().todolist.todos.find((todo) => todo.id === action.id).description }))
            .map((res) => patchTodoDescriptionSuccess(res.response))
            .catch((res) => patchTodoDescriptionFailure())
    )
}