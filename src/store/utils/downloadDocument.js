import { Observable } from 'rxjs/Rx'
import { download } from 'store/services/document'
import slugify from 'slugify'

const downloadDocument = (document, label, user) => {
  return Observable.ajax(download({ params: { id: document.id } }))
    .map((res) => res.response)
    .catch(() => Observable.never())
    .subscribe((blobby) => {
      let anchor = window.document.createElement('a')
      const objectUrl = window.URL.createObjectURL(blobby)

      window.document.body.appendChild(anchor)

      anchor.href = objectUrl
      anchor.download = generateDocumentName(document, label, user)
      anchor.target = '_self'
      anchor.click()

      window.URL.revokeObjectURL(objectUrl)
      anchor.remove()
    })
}

const generateDocumentName = (document, label, user) => {
  const fileExtension = document.fileName.split('.').pop()
  const documentName = `${user.firstname} ${user.lastname} ${label}.${fileExtension}`

  return slugify(documentName)
}

export default downloadDocument
