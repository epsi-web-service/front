
export function fetchAllTodo({token}) {
    return {
        url: `http://localhost:8080/todo-list/todo`,
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'text/plain',
        },
    }
}

export function postTodo({todo, token}) {

    return {
        url: `http://localhost:8080/todo-list/todo`,
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
        },
        body: todo
    }
}



export function patchTodoToggle({id, token}) {

    return {
        url: `http://localhost:8080/todo-list/todo/done/${id}`,
        method: 'PATCH',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
        },
        body: {}
    }
}


export function deleteTodo({id, token}) {

    return {
        url: `http://localhost:8080/todo-list/todo/${id}`,
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
        },
        body: {}
    }
}



export function patchTodoDescription({id, token, value}) {
    return {
        url: `http://localhost:8080/todo-list/todo/description/${id}`,
        method: 'PATCH',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
        },
        body: value
    }
}



