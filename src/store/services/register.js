export function register({ payload }) {

    return {
        url: `http://localhost:8080/user/api/register`,
        method: 'POST',
        headers: {
            Accept: 'application/ld+json',
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload),
    }
}
