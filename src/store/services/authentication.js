export function login({ payload }) {
  let data = new FormData()
  data.append('_username', payload.username)
  data.append('_password', payload.password)

  return {
    url: `http://localhost:8080/user/api/login`,
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
    },
    body: data,
  }
}

export function refresh({ params }) {
  let data = new FormData()
  data.append('refresh_token', params.token)

  return {
    url: `http://localhost:8080/user/api/token/refresh`,
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
    },
    body: data,
  }
}
