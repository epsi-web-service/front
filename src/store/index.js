import { createStore, compose, applyMiddleware } from 'redux'
import { createEpicMiddleware } from 'redux-observable'
import { routerMiddleware } from 'react-router-redux'
import Raven from 'utils/raven'
import createRavenMiddleware from 'raven-for-redux'
import createHistory from 'history/createBrowserHistory'
import { reducer, epic } from 'store/ducks'
import { initApp } from 'store/ducks/app'
import xhook from 'xhook'
import hooks from 'utils/hooks'

export const history = createHistory()

const middleware = compose(
  applyMiddleware(createEpicMiddleware(epic)),
  applyMiddleware(routerMiddleware(history)),
  applyMiddleware(createRavenMiddleware(Raven)),
)

// XHR request middlewares
if (hooks.before) {
  xhook.before((request) => hooks.before(request, store.getState()))
}

if (hooks.after) {
  xhook.after((response) => hooks.after(response, store.getState()))
}

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  middleware,
)

store.dispatch(initApp())

export default store
