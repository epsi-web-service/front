export const TYPES_FREIGHT_FORWARDER = 'FreightForwarder'
export const TYPES_INDEPENDENT_COURIER = 'IndependentCourier'
export const TYPES_COMPANY_COURIER = 'CompanyCourier'
export const TYPES_MANAGER = 'Manager'

export const TYPES = {
  FREIGHT_FORWARDER: TYPES_FREIGHT_FORWARDER,
  INDEPENDENT_COURIER: TYPES_INDEPENDENT_COURIER,
  COMPANY_COURIER: TYPES_COMPANY_COURIER,
  MANAGER: TYPES_MANAGER,
}

export const DOCUMENTS = {
  [TYPES_FREIGHT_FORWARDER]: [
    {
      label: 'components.smart.formVouchers.fields.kbisExtract',
      key: 'docKbisExtract',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.transportAttestation',
      key: 'docTransportAttestation',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.forwarderInsurance',
      key: 'docInsurance',
      required: true,
      type: 'file',
    },
  ],
  [TYPES_INDEPENDENT_COURIER]: [
    {
      label: 'components.smart.formVouchers.fields.kbisExtract',
      key: 'docKbisExtract',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.identityCard',
      key: 'docIdentityCard',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.insuranceDocument',
      key: 'docInsurance',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.transportLicense',
      key: 'docTransportLicense',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.companyStatus',
      key: 'docCompanyStatus',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.concealedWork',
      key: 'docConcealedWork',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.liabilityInsurance',
      key: 'docLiabilityInsurance',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.adrCertification',
      key: 'docAdrCertification',
      required: false,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.animalsCertification',
      key: 'docAnimalsCertification',
      required: false,
      type: 'file',
    },
  ],
  [TYPES_MANAGER]: [
    {
      label: 'components.smart.formVouchers.fields.kbisExtract',
      key: 'docKbisExtract',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.urssafAttestation',
      key: 'docUrssafAttestation',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.transportLicense',
      key: 'docTransportLicense',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.merchandiseInsurance',
      key: 'docMerchandiseInsurance',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.identityCard',
      key: 'docIdentityCard',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.companyStatus',
      key: 'docCompanyStatus',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.concealedWork',
      key: 'docConcealedWork',
      required: true,
      type: 'file',
    },
    {
      label: 'components.smart.formVouchers.fields.liabilityInsurance',
      key: 'docLiabilityInsurance',
      required: true,
      type: 'file',
    },
  ],
}
