const CONTACT = {
  EMAIL: 'contact@glinko.fr',
  PHONE: '02 28 85 36 71',
}

export default CONTACT
