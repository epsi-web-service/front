import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of'
import 'rxjs/add/operator/catch'

// Time in second to refresh a token before the expiration time of the current
export const REFRESH_TOKEN_TIME = 60 * 10

function catchUnauthorized(token, retry, halt) {
  return this.catch((error) => {
    let expired = new Date(token.exp * 1000).getTime() - new Date().getTime() / 1000 < REFRESH_TOKEN_TIME
    return Observable.of(error.message === 'Unauthorized' && expired ? retry(error) : halt(error))
  })
}

Observable.prototype.catchUnauthorized = catchUnauthorized
