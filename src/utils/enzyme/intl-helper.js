import React from 'react'
import { intlShape } from 'react-intl'
import { mount, shallow } from 'enzyme'
import { intl } from 'i18n'

function nodeWithIntlProp(node) {
  return React.cloneElement(node, { intl })
}

export function shallowWithIntl(node, { context, ...additionalOptions } = {}) {
  return shallow(nodeWithIntlProp(node), {
    context: Object.assign({}, context, { intl }),
    ...additionalOptions,
  })
}

export function mountWithIntl(node, { context, childContextTypes, ...additionalOptions } = {}) {
  return mount(nodeWithIntlProp(node), {
    context: Object.assign({}, context, { intl }),
    childContextTypes: Object.assign({}, { intl: intlShape }, childContextTypes),
    ...additionalOptions,
  })
}
