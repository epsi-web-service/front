import React from 'react'
import { intlShape } from 'react-intl'
import marksy from 'marksy'

const compile = marksy({
  createElement: React.createElement,
  /* eslint-disable react/display-name, react/prop-types */
  elements: {
    p: ({ children }) => <span>{children}</span>,
    b: ({ children }) => <b className="strong">{children}</b>,
    strong: ({ children }) => <strong className="strong">{children}</strong>,
  },
  /* eslint-disable react/display-name, react/prop-types */
})

const FormattedMarkdown = (props, context) => {
  const { id, defaultMessage, values, ...crumbs } = props
  const { intl } = context

  const compiled = compile(intl.formatMessage({ id, defaultMessage }, values)).tree.pop()

  return React.cloneElement(compiled, crumbs)
}

FormattedMarkdown.contextTypes = {
  intl: intlShape,
}

export default FormattedMarkdown
