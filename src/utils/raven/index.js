import Raven from 'raven-js'

if (process.env.NODE_ENV === 'beta') {
  Raven.config('https://9981ed3a2b964b51990907dcda913447@sentry.troopers.agency/19').install()
} else if (process.env.NODE_ENV === 'preproduction') {
  Raven.config('https://1304b36ea5cf4cdc837ac400944dd7fe@sentry.troopers.agency/18').install()
} else if (process.env.NODE_ENV === 'production') {
  Raven.config('https://2b3deea159464d44a61d1390ffb1af9e@sentry.troopers.agency/17').install()
}

export default Raven
