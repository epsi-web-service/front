import { addValidator, email } from 'redux-form-validators'

const phoneOrEmailValidator = addValidator({
  validator: function(options, value, allValues) {
    return /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(value) || email.REG_EMAIL.test(value) // eslint-disable-line
  },
})

export default phoneOrEmailValidator
