import { addValidator } from 'redux-form-validators'

const timeValidator = addValidator({
  validator: function(options, value, allValues) {
    return /^[0-9]{2}:[0-9]{2}$/.test(value) // eslint-disable-line no-useless-escape
  },
})

export default timeValidator
