import { addValidator } from 'redux-form-validators'

const phoneValidator = addValidator({
  validator: function(options, value, allValues) {
    return /^((\+)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/.test(value) // eslint-disable-line no-useless-escape
  },
})

export default phoneValidator
