import { addValidator } from 'redux-form-validators'

const postalCodeValidator = addValidator({
  validator: function(options, value, allValues) {
    return /^[0-9]{5}$/.test(value) // eslint-disable-line no-useless-escape
  },
})

export default postalCodeValidator
