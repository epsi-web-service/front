const hooks = {
  before: (request, state) => {
    if (request.url.match(/^\/api/)) {
      request.url = `${process.env.REACT_APP_API_SERVER}${request.url}`
    }

    if (typeof request.headers.Authorization !== 'undefined') {
      request.headers.Authorization = `Bearer ${state.authentication.token}`
    } else if (typeof request.headers.get === 'function' && request.headers.get('Authorization') !== null) {
      request.headers.set('Authorization', `Bearer ${state.authentication.token}`)
    }
  },
}

export default hooks
