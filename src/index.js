import React from 'react'
import { render as renderDOM } from 'react-dom'
import { Provider as ReduxProvider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import store, { history } from 'store'
import 'assets/css/styles.css'
import App from 'views/layouts/App'
import 'utils/polyfills'

const rootEl = document.querySelector('#app')

renderDOM(
  <ReduxProvider store={store}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
  </ReduxProvider>,
  rootEl,
)
