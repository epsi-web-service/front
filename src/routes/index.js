import withSecurity from 'views/enhancers/withSecurity'
import loadable from 'loadable-components'
import { TYPES } from 'constants/user'

const routes = [
  {
    path: '/',
    name: 'Login',
    component: loadable(() => import('views/pages/Login')),
    exact: true,
    display: true,
    sitemap: true,
  },
  {
      path: '/register',
      name: 'Register',
      component: loadable(() => import('views/pages/Register')),
      sitemap: true,
  },
  {
    path: '/logout',
    name: 'Logout',
    component: loadable(() => import('views/pages/Logout')),
    exact: true,
    sitemap: true,
  },
  {
    path: '/todolist',
    name: 'Company',
    component: withSecurity(loadable(() => import('views/pages/TodoList'))),
    exact: true,
  }
]

export default routes
