import { IntlProvider, addLocaleData } from 'react-intl'
import { updateIntl } from 'react-intl-redux'
import frLocaleData from 'react-intl/locale-data/fr'
// import enLocaleData from 'react-intl/locale-data/en'
import dotize from 'dotize'
import fr from 'i18n/fr.yaml'
// import en from 'i18n/en.yaml'

const current = localStorage.getItem('locale') || window.navigator.language.split('-').shift()
const fallback = 'fr'
const availables = [
  'fr',
  // 'en',
]

export const locale = availables.includes(current) ? current : fallback
export const messages = {
  fr: dotize.convert(fr),
  // en: dotize.convert(en),
}

localStorage.setItem('locale', locale)

addLocaleData([
  ...frLocaleData,
  // ...enLocaleData,
])

updateIntl({ locale, messages: messages[locale] })

const provider = new IntlProvider({ locale: locale, messages: messages[locale] }, {}).getChildContext()
export const intl = provider.intl
